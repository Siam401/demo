<?php
// app/Http/Controllers/DataController.php

namespace App\Http\Controllers;

use App\Models\Data;
use App\Traits\ResponseJSON;
use Illuminate\Http\Request;

class DataController extends Controller
{
    use ResponseJSON;

    public function store(Request $request)
    {
        $payload = json_decode($request->getContent(), true);

        // Assuming $payload is the decoded JSON data from the request

        Data::create([
            'shop_id' => $payload['shop_id'],
            'code' => $payload['code'],
            'success' => $payload['success'],
            'extra' => $payload['extra'],
            'data' => $payload['data'],
            'timestamp' => $payload['timestamp'],
        ]);

        return response()->json(['message' => 'Data stored successfully']);
    }

    public function index()
    {
        return $this->success()
            ->message('Data fetched successfully')
            ->response(Data::all());
    }
}
